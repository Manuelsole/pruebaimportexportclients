FROM php:8.0-apache

RUN apt-get update -y
RUN apt-get install git -y
RUN curl -sS https://getcomposer.org/installer | php -- --version=2.0.13 && \
	mv composer.phar /usr/local/bin/composer
RUN docker-php-ext-install pdo_mysql
COPY ./src /var/www/html/
RUN mkdir -p /var/www/html/public/dataoutput
RUN cd /var/www/html
RUN composer install