Import/Export Clients
======================

## Installing

1. Clone repo in you system with:
	```
	git clone https://Manuelsole@bitbucket.org/Manuelsole/pruebaimportexportclients.git [FOLDER]
	```

2. Go to folder and execute this commands:
	
	```
	docker-compose up -d
	```

3. Create a database with this commands:
	
	```
	docker exec -i [docker-container-name] php bin/console doctrine:database:create
	docker exec -i [docker-container-name] php bin/console doctrine:schema:update --force
	```
	
	examples:

	```
	docker exec -i prueba_apache_1 php bin/console doctrine:database:create
	docker exec -i prueba_apache_1 php bin/console doctrine:schema:update --force
	```
	
## Commands
Now you can execute the command for import/export clients with this:
	

	docker exec -i [docker-container-name] php bin/console clients-close-to-renew:import-form-extenal-source-and-export [csv | xml | db]
	
	examples:
	
	docker exec -i prueba_apache_1 php bin/console clients-close-to-renew:import-form-extenal-source-and-export csv
	docker exec -i prueba_apache_1 php bin/console clients-close-to-renew:import-form-extenal-source-and-export xml
	docker exec -i prueba_apache_1 php bin/console clients-close-to-renew:import-form-extenal-source-and-export db

	
This command with params **csv** and **xml** generate a respectives files in folder **app-data-output**

This command with param **db** persist clients in database.

Database params:

	db-name: prueba
	host: 127.0.0.1
	user: root
	pass: toor
	port: 3306
	
For run test you can execute this command:
	
	docker exec -i [docker-container-name] php ./vendor/bin/phpunit
	
	example:
	
	docker exec -i prueba_apache_1 php ./vendor/bin/phpunit