<?php


namespace App\Tests\ExportData;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\ExportData\ExportClientsFactory;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class ExportClientsFactoryTest extends KernelTestCase
{

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var CsvEncoder
     */
    private $csvEncoder;
    /**
     * @var XmlEncoder
     */
    private $xmlEncoder;
    /**
     * @var string
     */
    private $pathDataOutput;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->csvEncoder = new CsvEncoder();
        $this->xmlEncoder = new XmlEncoder();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $this->pathDataOutput= dirname(__FILE__) . '/../dataoutput';
    }

    public function testTryBuildAnExportClientNotDefinedInFactory(){
        $exportClientsFactory = new ExportClientsFactory(
            $this->csvEncoder,
            $this->xmlEncoder,
            $this->entityManager,
            $this->pathDataOutput
        );;
        $exportClientType = 'random';
        $this->expectException(\InvalidArgumentException::class);
        try {
            $exportClientsFactory->build($exportClientType);
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), "Type of export clients doesn't exist");
            throw $e;
        }
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown():void
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }
}