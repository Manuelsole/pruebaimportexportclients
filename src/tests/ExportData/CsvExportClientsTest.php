<?php


namespace App\Tests\ExportData;

use App\Entity\Client;
use App\ExportData\CsvExportClients;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Encoder\CsvEncoder;

class CsvExportClientsTest extends TestCase
{
    public function testDestinationFolderDoesNotExistOnExport(){
        $csvEncoder = new CsvEncoder();
        $destinationPath = dirname(__FILE__) . '/../not_exist';
        $csvExportClients = new CsvExportClients($csvEncoder, $destinationPath);
        $this->expectException(\InvalidArgumentException::class);
        $clients = new Client('','','','');
        try {
            $csvExportClients->exportClient($clients);
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), "Destination file path doesn't exits");
            throw $e;
        }
    }
}