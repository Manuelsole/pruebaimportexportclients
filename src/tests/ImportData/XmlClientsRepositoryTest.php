<?php


namespace App\Tests\ImportData;

use App\ImportData\XmlClientsRepository;
use PHPUnit\Framework\TestCase;

class XmlClientsRepositoryTest extends TestCase
{
    public function testEmpryArrayClientsFromImportEmptyXmlFile(){
        $path = dirname(__FILE__) . '/../datasource/empty_data.xml';
        $xmlClientRepository = new XmlClientsRepository($path);
        $clients = $xmlClientRepository->import();
        $this->assertEquals([], $clients);
    }
}