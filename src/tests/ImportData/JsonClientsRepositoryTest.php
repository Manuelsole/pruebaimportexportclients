<?php


namespace App\Tests\ImportData;

use App\ImportData\JsonClientsRepository;
use PHPUnit\Framework\TestCase;

class JsonClientsRepositoryTest extends TestCase
{
    public function testEmptyUrlParameterOnImport(){
        $url = '';
        $jsonClientRepository = new JsonClientsRepository($url);
        $this->expectException(\InvalidArgumentException::class);
        try {
            $jsonClientRepository->import();
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), "Url parameter on json import clients can't be empty");
            throw $e;
        }
    }
}