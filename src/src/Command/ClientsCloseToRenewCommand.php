<?php

namespace App\Command;


use App\ExportData\ExportClientsFactory;
use App\Service\ImportClientsCloseToRenew;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class ClientsCloseToRenewCommand extends Command
{
    protected static $defaultName = "clients-close-to-renew:import-form-extenal-source-and-export";

    private $importClientsCloseToRenewService;
    private $exportClientsFactory;

    public function __construct(importClientsCloseToRenew $importClientsCloseToRenewService, ExportClientsFactory $exportClientsFactory)
    {
        $this->importClientsCloseToRenewService = $importClientsCloseToRenewService;
        $this->exportClientsFactory = $exportClientsFactory;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription('generate a csv, xml or persist in bbdd clients that was close renew')
            ->setHelp('this command allows you to generate a csv, xml or persist in bbdd clients that was close renew')
            ->addArgument('typeExport', InputArgument::REQUIRED, 'Type of export');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('starting import from external data source');
        $clients = $this->importClientsCloseToRenewService->importFormExternalSources();
        $output->writeln('end import from external data source');

        $output->writeln('starting export with ' . $input->getArgument('typeExport') . ' method');
        $exportClient = $this->exportClientsFactory->build($input->getArgument('typeExport'));
        $exportClient->exportClient(...$clients);
        $output->writeln('end export with ' . $input->getArgument('typeExport') . ' method');

        return Command::SUCCESS;
    }
}