<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass=ClientRepository::class)
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $company;

    /**
     * Client constructor.
     * @param $name
     * @param $email
     * @param $phone
     * @param $company
     */
    public function __construct($name, $email, $phone, $company)
    {
        $this->id = Uuid::v4();
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
        $this->company = $company;
    }

    /**
     * @param $name
     * @param $email
     * @param $phone
     * @param $company
     * @return static
     */
    public static function create($name, $email, $phone, $company): self{
        return new self($name, $email, $phone, $company);
    }

    /**
     * @return array
     */
    public function exportDataCast(){
        return [
            $this->name,
            $this->email,
            $this->phone,
            $this->company
        ];
    }
}
