<?php

namespace App\Service;


use App\Entity\Client;
use App\ImportData\ClientsRepository;

class ImportClientsCloseToRenew
{
    /**
     * ImportClientsCloseToRenew constructor.
     * @param iterable $externalSourceData
     */
    public function __construct(private iterable $externalSourceData){}

    /**
     * @return Client[]
     */
    public function importFormExternalSources(){
        $clients = [];
        /** @var ClientsRepository $externalSourceData */
        foreach ($this->externalSourceData as $externalSourceData){
            $clients = array_merge($clients, $externalSourceData->import());
        }
        return $clients;
    }
}