<?php


namespace App\ExportData;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpClient\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class ExportClientsFactory
{
    /**
     * ExportClientsFactory constructor.
     * @param CsvEncoder $csvEncoder
     * @param XmlEncoder $xmlEncoder
     * @param EntityManagerInterface $entityManager
     * @param string $pathDataOutput
     */
    public function __construct(
        private CsvEncoder $csvEncoder,
        private XmlEncoder $xmlEncoder,
        private EntityManagerInterface $entityManager,
        private string $pathDataOutput){}

    /**
     * @param string $typeExport
     * @return ExportClients
     */
    public function build(string $typeExport): ExportClients
    {
        switch ($typeExport) {
            case 'csv':
                return new CsvExportClients($this->csvEncoder, $this->pathDataOutput);
            case 'xml':
                return new XmlExportClients($this->xmlEncoder, $this->pathDataOutput);
            case 'db':
                return new DatabaseExportClients($this->entityManager);
            default:
                throw new InvalidArgumentException("Type of export clients doesn't exist");
        }
    }
}
