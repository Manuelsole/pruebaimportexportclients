<?php


namespace App\ExportData;

use App\Entity\Client;
use Doctrine\ORM\EntityManagerInterface;

class DatabaseExportClients implements ExportClients
{
    /**
     * DatabaseExportClients constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(private EntityManagerInterface $entityManager){}

    /**
     * @param Client ...$clients
     */
    public function exportClient(Client ...$clients): void
    {
        foreach ($clients as $client){
            $this->entityManager->persist($client);
        }
        $this->entityManager->flush();
    }
}