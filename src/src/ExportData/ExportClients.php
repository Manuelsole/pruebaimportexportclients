<?php

namespace App\ExportData;

use App\Entity\Client;

interface ExportClients
{
    /**
     * @param Client ...$clients
     */
    function exportClient(Client ...$clients): void;
}