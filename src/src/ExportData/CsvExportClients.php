<?php


namespace App\ExportData;

use App\Entity\Client;
use Symfony\Component\Serializer\Encoder\CsvEncoder;

class CsvExportClients implements ExportClients
{
    /**
     * CsvExportClients constructor.
     * @param CsvEncoder $encoder
     * @param string $pathDataOutput
     */
    public function __construct(private CsvEncoder $encoder, private string $pathDataOutput){}

    /**
     * @param Client ...$clients
     */
    public function exportClient(Client ...$clients): void
    {
        if(!is_dir($this->pathDataOutput)){
            throw new \InvalidArgumentException("Destination file path doesn't exits");
        }

        foreach ($clients as $client) {
            $clientsExportCast[] = $client->exportDataCast();
        }

        file_put_contents(
            $this->pathDataOutput . 'data.csv',
            $this->encoder->encode($clientsExportCast, 'csv', [$this->encoder::NO_HEADERS_KEY => true])
        );
    }
}