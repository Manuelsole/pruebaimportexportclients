<?php


namespace App\ExportData;

use App\Entity\Client;
use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class XmlExportClients implements ExportClients
{
    /**
     * XmlExportClients constructor.
     * @param XmlEncoder $encoder
     * @param string $pathDataOutput
     */
    public function __construct(private XmlEncoder $encoder, private string $pathDataOutput){}

    /**
     * @param Client ...$clients
     */
    public function exportClient(Client ...$clients): void
    {
        if(!is_dir($this->pathDataOutput)){
            throw new \InvalidArgumentException("Destination file path doesn't exits");
        }

        foreach ($clients as $client) {
            $clientsExportCast[] = $client->exportDataCast();
        }

        file_put_contents(
            $this->pathDataOutput . 'data.xml',
            $this->encoder->encode($clientsExportCast, 'xml')
        );
    }
}