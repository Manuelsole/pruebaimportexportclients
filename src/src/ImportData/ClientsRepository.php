<?php

namespace App\ImportData;

use App\Entity\Client;

interface ClientsRepository
{
    /**
     * @return Client[]
     */
    function import(): array;
}