<?php


namespace App\ImportData;

use App\Entity\Client;

class JsonClientsRepository implements ClientsRepository
{
    /**
     * JsonClientsRepository constructor.
     * @param string $url
     */
    public function __construct(private string $url){}

    /**
     * @return Client[]
     */
    public function import(): array
    {
        if(empty($this->url)){
            throw new \InvalidArgumentException("Url parameter on json import clients can't be empty");
        }

        $clients = [];
        $infoClients = json_decode(file_get_contents($this->url));
        if(!empty($infoClients)){
            foreach($infoClients as $infoClient){
                $clients[] = Client::create(
                    $infoClient->name,
                    $infoClient->email,
                    $infoClient->phone,
                    $infoClient->company->name
                );
            }
        }

        return $clients;
    }
}