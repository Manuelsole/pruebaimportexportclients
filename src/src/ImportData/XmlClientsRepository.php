<?php


namespace App\ImportData;

use App\Entity\Client;

class XmlClientsRepository implements ClientsRepository
{
    /**
     * XmlClientsRepository constructor.
     * @param string $filePath
     */
    public function __construct(private string $filePath){}

    /**
     * @return Client[]
     */
    public function import(): array
    {
        if(!file_exists($this->filePath)){
            throw new \InvalidArgumentException('xml file not found');
        }

        $clients = [];

        $infoClients = simplexml_load_file($this->filePath);
        if(!empty($infoClients)){
            foreach($infoClients as $infoClient){
                $clients[] = Client::create(
                    $infoClient['name']->__toString(),
                    $infoClient->__toString(),
                    $infoClient['phone']->__toString(),
                    $infoClient['company']->__toString()
                );
            }
        }

        return $clients;
    }
}